#include <stdio.h>
#define MAX_CHAR 80
#define SIZE 8

typedef enum{
	white = 'X', black = 'Y'
}colours;

struct player{
	char name[MAX_CHAR];
	char disc_colour;//keeps track of the player's disc colour
	int amount_discs;//keeps track of how many discs the player has
	};
	
struct disc{
	colours type; //ref to enum
	int xaxis; //number of rows
	int yaxis; //number  of columns
};
	
void display(char board[][SIZE]);

int main(void)
{
	int players;
	char board [SIZE][SIZE] = {{0}}; //8x8 size board
	int i, j;
	
	//initialise player 1
	struct player player1;
	printf("Enter name");
	fgets(player1.name, MAX_CHAR, stdin);
	player1.disc_colour = white;
	printf("Player has white discs - %c\n", white);
	player1.amount_discs =  2; //2 discs per player at the start
	
	
	struct player player2;
	printf("Enter name");
	fgets(player2.name, MAX_CHAR, stdin);
	player2.disc_colour = black;
	printf("Player has black discs - %c\n", black);
	player2.amount_discs =  2;
	
	/*Starting position of the board*/
	for(i=0;i<SIZE;i++)
		for(j=0;j<SIZE;j++)
		{
			board[i][j] = ' '; /* Set every square on the board as empty*/
		}
		
		(board[(SIZE/2) - 1][SIZE/2-1]) = (board[SIZE/2][SIZE/2]) = white;
			
		(board[SIZE/2-1][SIZE/2]) = (board[SIZE/2][SIZE/2-1]) = black;		
		 
		 display(board); /*Displays current state of the board*/
		 	 
		return 0; 
}
	
void display(char board[][SIZE])
 {	
	int i,j;
	
	
	printf("\n ");
	for(j=0;j<8;j++) /*Numbers across the top*/
		printf(" %2d ", j+1);
	printf("\n");
	
	
	for(i=0;i<8;i++) 
	{
		printf( "\n%d |", i+1); /*Numbers down the sides*/
		
		for(j=0;j<8;j++)
				printf(" %c |", board[i][j]);
			printf("\n");
		
	}
 }