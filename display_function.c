#include <stdio.h>
#define MAX_CHAR 80
#define SIZE 8
#include "headers.h"

void display(char board[][SIZE])
 {	
	int i,j;
	
	
	printf("\n ");
	for(j=0;j<8;j++) /*Numbers across the top*/
		printf(" %2d ", j+1);
	printf("\n");
	
	
	for(i=0;i<8;i++) 
	{
		printf( "\n%d |", i+1); /*Numbers down the sides*/
		
		for(j=0;j<8;j++)
				printf(" %c |", board[i][j]);
			printf("\n");
		
	}
 }
