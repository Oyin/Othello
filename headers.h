#include <stdio.h>
#define MAX_CHAR 80
#define SIZE 8

typedef enum{
	white = 'X', black = 'Y'
}colours;

struct player{
	char name[MAX_CHAR];
	char disc_colour;//keeps track of the player's disc colour
	int amount_discs;//keeps track of how many discs the player has
	};
	
struct disc{
	colours type; //ref to enum
	int xaxis; //number of rows
	int yaxis; //number  of columns
};
	
void display(char board[][SIZE]);